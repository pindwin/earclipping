﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace pindwin.EarClipping
{
	public class Polygon
	{
        private readonly List<Vector3> _points;
		private Vector3 _avg;
		private Vector3 _normal;
		private bool _isCounterClockwise;

		private const float EPSILON = 0.0001f;

		public bool HasNormal => _points.Count >= 3;
		public IReadOnlyList<Vector3> Points => _points;
		public Vector3 Normal => _normal;
		public bool IsCounterClockwise => _isCounterClockwise;

		public Polygon()
		{
			_points = new List<Vector3>();
		}

		public Polygon(IEnumerable<Vector3> points)
		{
			_points = new List<Vector3>(points);
			for (int i = 3; i < _points.Count; i++)
			{
				if (IsPointOnPlane(_points[i]) == false)
				{
					throw new ArgumentException($"Point with index {i} does not fall on a plane.");
				}
			}

			if (_points.Count >= 3)
			{
				EstablishPlane(_points[0] + Vector3.up);
			}
		}

		/// <summary>
		/// Adds a point to polygon. First 3 points will always be accepted - since polygon normal will not be known yet.
		/// After placing 3 points, you need to make sure that point is coplanar with first few points - otherwise, method
		/// will return <c>false</c>.
		/// </summary>
		/// <returns><c>true</c> if point was accepted, <c>false</c> otherwise.</returns>
		public bool AddPoint(Vector3 point, Vector3 cameraPosition)
		{
			if (_points.Count < 3)
			{
				_points.Add(point);
				if (_points.Count == 3)
				{
					EstablishPlane(cameraPosition);
				}
				return true;
			}

			if (IsPointOnPlane(point) == false)
			{
				Debug.LogError("Point does not fall on the same plane and won't be added.");
				return false;
			}

			_points.Add(point);
			return true;
		}

		/// <summary>
		/// Performs virtual (non-Unity) raycast on a polygons plane and returns hit position.
		/// </summary>
		/// <param name="perspective">Point of view - usually camera position</param>
		/// <returns>Raycast hit position or <c>Vector3.zero</c>, if polygon had less than 3 points</returns>
		public Vector3 GetCoplanarPoint(Vector3 perspective, Ray ray)
		{
			if (HasNormal == false)
			{
				Debug.LogError("Cannot GetCoplanarPoint for polygon with less than 3 points - plane unknown yet.");
				return Vector3.zero;
			}

			float dist = Vector3.Dot(_points[0] - perspective, _normal) / Vector3.Dot(ray.direction, _normal);
			return perspective + ray.direction * dist;
		}

		/// <summary>
		/// Gets coplanar point if possible, then adds it to Points, skipping coplanar checks.
		/// No point will be added if polygon had less then 3 points before call.
		/// </summary>
		/// <param name="perspective">Point of view - usually camera position</param>
		/// <returns>Added point or <c>Vector3.zero</c>, if polygon had less than 3 points</returns>
		public Vector3 AddCoplanarPoint(Vector3 perspective, Ray ray)
		{
			if (HasNormal == false)
			{
				Debug.LogError("Cannot AddCoplanarPoint for polygon with less than 3 points - plane unknown yet.");
				return Vector3.zero;
			}

			var point = GetCoplanarPoint(perspective, ray);
			_points.Add(point);
			return point;
		}

		/// <summary>
		/// Modifies a point under an index. If there are 3 or less points when this is called, the normal will be modified and
		/// call will always succeed. If there are 4 or more points, the new point needs to adhere to preexisting plane.
		/// </summary>
		public bool ModifyPoint(int index, Vector3 point)
		{
			if (index < 0 || index > _points.Count)
			{
				return false;
			}

			if (_points.Count <= 3)
			{
				_points[index] = point;
				if (_points.Count == 3)
				{
					EstablishPlane(point + _normal);
				}
				return true;
			}

			if (IsPointOnPlane(point) == false)
			{
				Debug.LogError("Point does not fall on the same plane and won't be added.");
				return false;
			}

			_points[index] = point;
			return true;
		}

		/// <summary>
		/// Removes pre-existing point from a polygon. This may invalidate normal, if performed while <c>Points.Count == 3</c>
		/// </summary>
		public bool RemovePoint(int index)
		{
			if (index < 0 || index >= _points.Count)
			{
				return false;
			}

			_points.RemoveAt(index);
			return true;
		}

		/// <summary>
		/// Checks, whether polygon is simple, i.e. none of its edges cross. Non-simple polygon cannot be triangulated
		/// using EarClipping algorithm.
		/// </summary>
		public bool IsSimple()
		{
			for (int i = 1; i < _points.Count; i++)
			{
				Vector3 start = _points[i - 1];
				Vector3 dir = _points[i] - start;

				for (int j = i + 1; j < _points.Count && (j + 1) % _points.Count != i - 1; j++)
				{
					Vector3 otherStart = _points[j];
					Vector3 otherDir = _points[(j + 1) % _points.Count] - otherStart;
					if (FindLinesIntersection(out var intersection, start, dir, otherStart, otherDir)
						&& IsPointOnASegment(intersection, start, dir)
						&& IsPointOnASegment(intersection, otherStart, otherDir))
					{
						return false;
					}
				}
			}

			return true;
		}

		/// <summary>
		/// Performs EarClipping algorithm and stores triangles topology in a passed list.
		/// </summary>
		public void Triangulate(List<int> triangles)
		{
			triangles.Clear();
			List<int> indices = Enumerable.Range(0, _points.Count).ToList();
			DoEarClipping(indices, triangles);
		}

		private bool IsPointOnPlane(Vector3 point)
		{
			Vector3 checkedNormal = (Vector3.Cross(_points[1] - _points[0], point - _points[0])).normalized;
			return Mathf.Abs(1 - Mathf.Abs(Vector3.Dot(checkedNormal, _normal))) <= EPSILON;
		}

		private void EstablishPlane(Vector3 cameraPosition)
		{
			_avg = ((_points[0] + _points[1] + _points[2]) / 3);
			var cameraDir = cameraPosition - _avg;
			cameraDir.Normalize();
			
			_normal = Vector3.Cross(_points[0] - _points[1], _points[2] - _points[1]);
			_normal.Normalize();
			if (Vector3.Dot(_normal, cameraDir) > 0)
			{
				_isCounterClockwise = true;
			}
		}

		private void DoEarClipping(List<int> indices, List<int> tris)
		{
			if (indices.Count <= 3)
			{
				if (_isCounterClockwise)
				{
					indices.Reverse();
				}
				tris.AddRange(indices);
				return;
			}

			for (int i = 0; i < indices.Count; i++)
			{
				if (IsIndexAnEar(i))
				{
					var ear = GetEarForIndex(i);
					tris.Add(_isCounterClockwise ? indices[ear.z] : indices[ear.x]);
					tris.Add(indices[ear.y]);
					tris.Add(_isCounterClockwise ? indices[ear.x] : indices[ear.z]);
					indices.RemoveAt(i);
					DoEarClipping(indices, tris);
					return;
				}
			}

			bool IsIndexAnEar(int index)
			{
				var ear = GetEarForIndex(index);
				Vector3 first = (_points[indices[ear.y]] - (_isCounterClockwise ? _points[indices[ear.z]] : _points[indices[ear.x]])).normalized;
				Vector3 second = ((_isCounterClockwise ? _points[indices[ear.x]] : _points[indices[ear.z]]) - _points[indices[ear.y]]).normalized;
				Vector3 localRight = Vector3.Cross(_normal, first);
				if (_isCounterClockwise == false && Vector3.Dot(second, localRight) > 0)
				{
					return false;
				}

				if (_isCounterClockwise && Vector3.Dot(second, localRight) < 0)
				{
					return false;
				}

				Vector3 baseA = _points[indices[ear.x]];
				Vector3 baseB = _points[indices[ear.y]];
				Vector3 baseC = _points[indices[ear.z]];

				for (int i = 2; i < indices.Count - 1; i++)
				{
					var point = _points[indices[(index + i) % indices.Count]];

					if (IsPointWithinTriangle(point, baseA, baseB, baseC))
					{
						return false;
					}
				}

				return true;
			}

			Vector3Int GetEarForIndex(int index)
			{
				return new Vector3Int(
						((index - 1) + indices.Count) % indices.Count,
						index,
						(index + 1) % indices.Count
					);
			}
		}

		private static bool FindLinesIntersection(out Vector3 intersection, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2)
		{
			Vector3 lineVec3 = linePoint2 - linePoint1;
			Vector3 crossVec1and2 = Vector3.Cross(lineVec1, lineVec2);
			Vector3 crossVec3and2 = Vector3.Cross(lineVec3, lineVec2);
 
			float planarFactor = Vector3.Dot(lineVec3, crossVec1and2);
 
			//is coplanar, and not parrallel
			if (Mathf.Abs(planarFactor) < EPSILON && crossVec1and2.sqrMagnitude > EPSILON)
			{
				float s = Vector3.Dot(crossVec3and2, crossVec1and2) / crossVec1and2.sqrMagnitude;
				intersection = linePoint1 + (lineVec1 * s);
				return true;
			}
			else
			{
				intersection = Vector3.zero;
				return false;
			}
		}

		private static bool IsPointOnASegment(Vector3 intersection, Vector3 segStart, Vector3 segVec)
		{
			var segEnd = segStart + segVec;
			if (segStart == intersection || segEnd == intersection)
			{
				return true;
			}
			float distToStart = (segStart - intersection).magnitude;
			float distToEnd = (segEnd - intersection).magnitude;
			var dist = segVec.magnitude - (distToStart + distToEnd);
			return Mathf.Abs(dist) < 0.001;
		}

		private static bool IsPointWithinTriangle(Vector3 point, Vector3 v1, Vector3 v2, Vector3 v3)
		{
			return 
				AreOnTheSameSide(point, v1, v2, v3 - v2) 
				&& AreOnTheSameSide(point, v2, v1, v3 - v1) 
				&& AreOnTheSameSide(point, v3, v1, v2 - v1);

			bool AreOnTheSameSide(Vector3 p1, Vector3 p2, Vector3 start, Vector3 dir)
			{
				var cp1 = Vector3.Cross(dir, p1 - start);
				var cp2 = Vector3.Cross(dir, p2 - start);
				return Vector3.Dot(cp1, cp2) >= 0;
			}
		}
	}
}
