Run an [Example] scene to see the algorithm in action, consult [ExampleSript.cs] to see usage.

Math behind an algorith can be found e.x. here: https://www.geometrictools.com/Documentation/TriangulationByEarClipping.pdf

The asset does not support holes in polygons (at least yet).

Questions? Get in touch: contact@pindwin.com