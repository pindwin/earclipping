using System.Collections.Generic;
using UnityEngine;

namespace pindwin.EarClipping.Example
{
    public class ExampleScript : MonoBehaviour
    {
		[SerializeField] GameObject _sphere;
		[SerializeField] GameObject _marker;

		Color[] _colors = new Color[] {Color.red, Color.green, Color.blue, Color.yellow, Color.cyan, Color.magenta, Color.white, Color.gray, Color.black};
		private Polygon _polygon;
		private Transform _cameraTransform;
		private Camera _camera;

		private void Start()
		{
			_polygon = new Polygon();
			_camera = Camera.main;
			_cameraTransform = _camera.transform;
		}

		private void Update()
        {
			if (Input.GetKeyDown(KeyCode.Mouse0))
			{
				if (_polygon.Points.Count < 3)
				{
					if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out var hit, 100.0f) == false)
					{
						return;
					}

					if(_polygon.AddPoint(hit.point, _cameraTransform.position))
					{
						Instantiate(_marker, hit.point, Quaternion.identity);
					}

					if (_polygon.HasNormal)
					{
						_sphere.SetActive(false);
					}
					return;
				}

				Vector3 pos = _polygon.AddCoplanarPoint(_cameraTransform.position, _camera.ScreenPointToRay(Input.mousePosition));
				Instantiate(_marker, pos, Quaternion.identity);
			}

			if (Input.GetKeyDown(KeyCode.Mouse1) && _polygon.HasNormal)
			{
				if (_polygon.IsSimple() == false)
				{
					Debug.LogError("Can't do ear clipping on a non-simple polygon!");
					return;
				}

				List<int> triangles = new List<int>();
				_polygon.Triangulate(triangles);

				
				GameObject go = new GameObject("Result");
				int c = 0;
				List<Vector3> points = new List<Vector3>(_polygon.Points);

				for (int i = 0; i < triangles.Count - 2; i += 3)
				{
					GameObject triangle = new GameObject($"Triangle {i / 3 + 1}");
					triangle.transform.parent = go.transform;
					var mf = triangle.AddComponent<MeshFilter>();
					var mr = triangle.AddComponent<MeshRenderer>();

					Mesh m = new Mesh();
					m.SetVertices(points);
					m.SetTriangles(triangles.GetRange(i, 3), 0);
					mr.material = _marker.GetComponent<Renderer>().sharedMaterial;
					mr.material.color = _colors[c++ % _colors.Length];

					mf.mesh = m;
				}
			}
        }

	}
}

